var React = require('react');
var _ = require('lodash');
var SortHeader = React.createClass({
        handleChange: function(e) {
            this.props.onChange(e.target.name, e.target.value)
        },
        render: function() {
            var handleChange = this.handleChange;

            var tableHeads = _.map(this.props.cols, function (col, i) {
                return (<th key={i}>
                            <ul className="sort-order">
                                <li onClick={this.props.onClick.bind(null, col,'up')}>&and;</li>
                                <li onClick={this.props.onClick.bind(null, col,'down')}>&or;</li>
                            </ul>
                            {col}
                            <input type="text" name={col} onChange={this.handleChange} />
                        </th> );
            }, this);
            return (<thead>
                        {tableHeads}
                    </thead>)
        }
});
module.exports = SortHeader;
