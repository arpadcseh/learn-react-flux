var _ = require('lodash');
var zepto = require('zeptojs');
var SortHeader = require('./sortheader');
var TableRows = require('./tablerows');
var React = require('react');
// Global
var DATASET = [];

var TableSort =
    React.createClass({
        getInitialState: function(){
            return {rows: '', cols: '', appliedFilters: {}}
        },
        componentDidMount: function() {
            var exceptColsStr = this.props.exceptCols;
            var exceptCols = exceptColsStr.split(" ");
            zepto.getJSON(this.props.source, function(result) {
              if (this.isMounted()) {
                // omit except cols
                DATASET = _.map(result, function(r){ return _.omit(r, exceptCols)});
                // set rows & cols
                this.setState({ rows: DATASET, cols: _.keys(DATASET[0]) });
              }
            }.bind(this));
        },
        sortOrder: function(col, order) {
            var orderedRows = (order === 'up') ? _.sortBy(this.state.rows, col) : _.sortBy(this.state.rows, col).reverse();
            this.setState({ rows: orderedRows});
        },
        filterBy: function(col, value) {
            // store filters
            var appliedFilters = this.state.appliedFilters;
            appliedFilters[col] = value;
            // apply filters
            var filteredRows = [], run = 1;
            _.forEach(appliedFilters, function(value, key){
                var exp = new RegExp(value, "i"); 
                filterFrom = (run === 1) ? DATASET : filteredRows;
                filteredRows = _.filter(filterFrom, function(row){ 
                    return exp.test(row[key])
                });
                run++; 
            });
            //set rows & filters
            this.setState({ rows: filteredRows,
                appliedFilters: appliedFilters});

        },
        render: function(){
            var sortOrder = this.sortOrder,
                filterBy = this.filterBy;

            return <table>
                       <SortHeader cols={this.state.cols} onClick={sortOrder} onChange={filterBy} />
                       <TableRows cols={this.state.cols} rows={this.state.rows}/>
                   </table>
        }
    });
module.exports = TableSort;
