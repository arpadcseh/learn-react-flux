var React = require('react');
var _ = require('lodash');

var TableRows = React.createClass({
  render: function() {
    var tableRows = _.map(this.props.rows, function (row) {
        var rowTds = _.map(row, function (rowTd, i) {
                        return ( <td key={i}>{rowTd}</td> );
                    });
        return (<tr key={row.index}>
                {rowTds}
            </tr>);
    });
    return (<tbody>
                {tableRows}
            </tbody>);
  }
});
module.exports = TableRows;
