var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    connect = require('gulp-connect');

gulp.task('browserify', function() {
    gulp.src('src/js/main.js')
        .pipe(browserify({transform: 'reactify'}))
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(connect.reload())
});
gulp.task('copy', function() {
    gulp.src('src/*.*')
        .pipe(gulp.dest('dist'))
        .pipe(connect.reload())
});
gulp.task('connect', function() {
    connect.server({
        root: 'dist',
        livereload: true
    })
});
gulp.task('build',['browserify', 'copy']);
gulp.task('default',['connect', 'watch']);

gulp.task('watch', function() {
    gulp.watch('src/**/*.*', ['build']);
});
